from django.db import models
from django.contrib.auth.models import User
from stock.models import Track


class UserDetail(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='detail')
    favory = models.ManyToManyField(Track, null=True, blank=True)
    image = models.FileField(upload_to=f"media/stock/album/", default="media/stock/album/avatar.webp", null=False)
    phone = models.CharField(max_length=100, default='')
    genre = models.CharField(max_length=10, default='')
    state = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    birth_day = models.DateField(auto_now=True, auto_created=True)
    is_artist = models.BooleanField(default=False)
    is_subscriber = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
