from django.urls import path
from rest_framework.routers import SimpleRouter

from . import views as vs

viewsets = (vs.UserViewSet, vs.UserDetailViewSet)

router = SimpleRouter()
for viewset in viewsets:
    router.register(r'{}'.format(viewset.url_prefix), viewset, basename=viewset.url_prefix)

urlpatterns = [
    path('token/', vs.login_view, name='token_obtain'),  # POST: {"username":"","password": ""}
    path('register/', vs.register, name='register'),
    path('update/', vs.update, name='update')
    
]
urlpatterns += router.urls
