from django.urls import re_path

from .consumers import OnlineConsumer

websocket_urlpatterns = [
    # re_path(r'ws/chat/(?P<room_name>\w+)/$', OnlineConsumer.as_asgi()),
    re_path(r'ws/onlines/(?P<room_name>\w+)/$', OnlineConsumer.as_asgi()),
]
