from django.db import models
from django.contrib.auth.models import User


class UserDetail(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='detail')
    image = models.TextField()
    phone = models.CharField(max_length=100)
    genre = models.CharField(max_length=3)
    state = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    birth_day = models.DateField(auto_now=True)
    is_artist = models.BooleanField(default=False)
    is_subscriber = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
