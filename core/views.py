from distutils.command import install_headers
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated

from .authentification import authentificates
from .custom_responses import GoodResponse, BadRequest
from .serializers import UserSerializer, UserDetailSerializer
from .custom_viewset import CRUDViewset
from .models import UserDetail


@api_view(['POST'])
@permission_classes([AllowAny])
def login_view(request):
	try:
		access_token, refresh_token = authentificates(email=request.data['email'], password=request.data['password'])
	except Exception as e:
		return BadRequest(e)
	data = {'access': access_token, 'refresh': refresh_token}
	return GoodResponse(data)


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
	data = request.data
	user = User.objects.create_user(
		username=data['username'],
		email=data['email'],
		password=data['password'],
		is_superuser=data['is_admin'] if 'is_admin' in data else False,
	)
	user.save()
	user_detail = UserDetail.objects.create(
		is_artist=request.data['is_artist'],
		user=user
	)
	user_detail.save()
	return GoodResponse({'detail': "succefull add"})


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update(request):
	data = request.data

	user = User.objects.get(pk=data['pk'])
	user.username = data['username']
	user.email = data['email']
	user.password = data['password']
	user.is_superuser = data['is_admin'] if 'is_admin' in data else False
	if 'password' in data:
		user.set_password(data['password']) if data['password'] != '' else None
	user.save()

	user_detail = UserDetail.objects.get(user=user)
	user_detail.is_artist = request.data['is_artist']
	user_detail.save()

	return GoodResponse({'detail': "succefull updated"})


class UserViewSet(CRUDViewset):
	url_prefix = 'user'
	queryset = User.objects.all()
	serializer_class = UserSerializer
	# permission_classes = [IsAuthenticated]

class UserDetailViewSet(CRUDViewset):
	url_prefix = 'user-detail'
	queryset = UserDetail.objects.all()
	serializer_class = UserDetailSerializer
	# permission_classes = [IsAuthenticated]
