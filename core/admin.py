from django.contrib import admin
from .models import UserDetail


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone', 'genre', 'state', 'city', 'birth_day', 'is_artist', 'is_subscriber', 'created_at', 'updated_at')

admin.site.register(UserDetail, AuthorAdmin)
