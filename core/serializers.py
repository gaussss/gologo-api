from rest_framework import serializers
from django.contrib.auth.models import User
from .models import UserDetail


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDetail
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    detail = UserDetailSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'is_superuser', 'pk', 'detail']
