from random import randrange
from tkinter.ttk import Style

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import FieldError


class Category(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name


class Album(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField(default='Aucune description')
    image = models.FileField(upload_to=f"media/stock/album/")
    auteur = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.name

    @classmethod
    def get_none(cls):
        return None

    def total_track(self):
        tracks = Track.objects.filter(album=self)
        return len(tracks)

    def total_track_see(self):
        total = 0
        tracks = Track.objects.filter(album=self)
        for track in tracks:
            total += track.see
        return total

    @classmethod
    def get_albums_order_by_most_see(cls):
        verif = False
        see_album = [[album, album.total_track_see] for album in cls.objects.all()]
        while verif:
            verif = True
            for i in range(len(see_album)):
                if see_album[i][1] > see_album[i+1][1]:
                    verif = False
                    _ = see_album[i][1]
                    see_album[i][1] = see_album[i+1][1]
                    see_album[i+1][1] = _
        return [i[0] for i in see_album]


class Track(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField(default='Aucune description')
    song = models.FileField(upload_to=f"media/stock/track")
    style = models.ManyToManyField(Category)
    duration = models.FloatField(default=180.0)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='tracks')
    is_active = models.BooleanField(default=True)
    see = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title
    
    @classmethod
    def add_see(cls, pk):
        track = cls.objects.get(pk=pk)
        track.see += 1
        track.save()

    @classmethod
    def get_track_order_by_most_see(cls):
        return cls.models.order_by('see').desc()
    
    @classmethod
    def get_track_order_by_created_date(cls):
        return cls.objects.all().order_by('created_at').asc()

    @classmethod
    def delet(cls, pk):
        track = cls.objects.get(pk=pk)
        track.delete()


class History(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    track = models.ForeignKey(Track, null=True, on_delete=models.SET_NULL)
    time = models.FloatField(default=0.0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PlayList(models.Model):
    name = models.CharField(max_length=500)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    tracks = models.ManyToManyField(Track, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class HomeDisc(models.Model):
    name = models.CharField(max_length=500)
    user = models.ManyToManyField(User, null=True, blank=True)
    # image = models.FileField(upload_to=f"media/stock/album/")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
