# Generated by Django 3.2.11 on 2022-02-20 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0008_history_time'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='time',
            field=models.FloatField(default=0.0),
        ),
    ]
