from django.contrib import admin
from .models import Track, Album, History, Category

@admin.register(Track)
class TrackAdmin(admin.ModelAdmin):
    list_display = ('title', 'duration', 'description',
                    'song', 'album',
                    'is_active', 'see', 
                    'created_at', 'updated_at')
    list_per_page = 10

@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',
                    'image', 'auteur', 'is_active',
                    'created_at', 'updated_at')
    list_per_page = 10


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'track',
                    'time', 'created_at', 'updated_at')
    list_per_page = 10


@admin.register(Category)
class Category(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at')
    list_per_page = 10
