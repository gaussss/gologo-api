from unittest import result
import numpy as np
import datetime
from math import exp, tanh
from threading import Thread


class ResolveSimilarityThread(Thread):
    
    def __init__(self, user_id, vect, data, cos_similarity_func) -> None:
        self.user_id, self.vect = user_id, vect
        self.data = data
        self.cos_similarity_func = cos_similarity_func
        self.result = []
        Thread.__init__(self)
    
    def run(self) -> any:
        user_to_user_similarity_list = []
        for _user_id, _vect in self.data:
            user_to_user_similarity = \
                self.cos_similarity_func(np.array(self.vect)[:, 1], np.array(_vect)[:, 1])
            user_to_user_similarity_list.append([_user_id, user_to_user_similarity])
        user_to_user_similarity_list = \
            sorted(user_to_user_similarity_list, key=lambda x: x[1], reverse=True)
        self.result = [self.user_id, user_to_user_similarity_list]



class FiltrageColab(object):
    
    def __init__(self, user_class, track_class, history_class):
        self.user = user_class
        self.track = track_class
        self.history = history_class
        
        self.history_list = []
        self.user_list = None
        self.track_list = None
        self.data = None
        self.model = {}
        self.transposed_data = None
    
    def load_data(self):
        self.track_list = self.track.objects.all()
        self.user_list = self.user.objects.all()
        for history in self.history.objects.all():
            try:
                history.user.pk
                history.track.pk
                self.history_list.append(history)
            except:
                pass
    
    def preprocess_data(self):
        result = []
        for user in self.user_list:
            user_track_note_list = []
            for track in self.track_list:
                note_user_track = self.resolve_note(user.pk, track.pk)
                user_track_note_list.append([track.pk, note_user_track])
            result.append([user.pk, user_track_note_list])
        self.data = result
    
    def get_user(self, user_pk, user_list):
        user = [user_item for user_item in user_list if user_item.pk == user_pk]
        if user:
            return user[0]
        else:
            raise Exception("user not found")
    
    def get_history(self, history_pk, history_list):
        history = [h_item for h_item in history_list if h_item.pk == history_pk]
        if history:
            return history[0]
        else:
            raise Exception("history not found")
    
    def get_track(self, track_pk, track_list):
        track = [t_item for t_item in track_list if t_item.pk == track_pk]
        if track:
            return track[0]
        else:
            raise Exception("history not found")
    
    def get_history_user_track(self, user_pk, track_pk, history_list):
        history = [h_item for h_item in history_list \
            if h_item.user.pk == user_pk and h_item.track.pk == track_pk]
        if history:
            return history
        else:
            return []
    
    def get_history_user(self, user_pk, history_list):
        history = [h_item.track.pk for h_item in history_list \
            if h_item.user.pk == user_pk]
        if history:
            return history
        else:
            return []
    
    def temporal_weight(self, date, func="ld"):
        old_date = datetime.datetime.strptime(date.isoformat()[:10], '%Y-%m-%d')
        actual_date = datetime.datetime.now()
        diff = actual_date - old_date

        beta = 15
        alpha = 0.1
        tanhy = lambda x: 1/2*(1 - tanh(alpha*(x-beta)))
        logistic_derease = lambda x: 1 - 1/(1 + exp(-alpha*(x-beta)))

        # print(diff.total_seconds())
        if func == "ld":
            return logistic_derease(diff.days)
        elif func == "tanh":
            return tanhy(diff.days)
    
    def resolve_note(self, user_pk, track_pk):
        user = self.get_user(user_pk, self.user_list)
        track = self.get_track(track_pk, self.track_list)
        history_user = self.get_history_user_track(user_pk, track_pk, self.history_list)
        history_user_length = len(history_user)
        if history_user_length==0:
            return 0
        note = np.sum(np.array([history.time/history.track.duration \
                for history in history_user if history.track!=None]))/history_user_length
        return note
    
    def resolve_data_similarity(self):
        # result = []
        # user_to_user_similarity_list = []
        # for user_id, vect in self.data:
        #     for _user_id, _vect in self.data:
        #         user_to_user_similarity = \
        #             self.cos_similarity_vect(np.array(vect)[:, 1], np.array(_vect)[:, 1])
        #         user_to_user_similarity_list.append([_user_id, user_to_user_similarity])
        #     user_to_user_similarity_list = \
        #         sorted(user_to_user_similarity_list, key=lambda x: x[1], reverse=True)
        #     result.append([user_id, user_to_user_similarity_list])

        step = len(self.data)//2
        step = step if step != 0 else 1
        parts = [self.data[i: i+step] for i in range(0, len(self.data), step)]
        result = []
        threads = []
        for user_id, vect in self.data:
            threads.append([
                        ResolveSimilarityThread(user_id, vect, part, self.cos_similarity_vect)\
                        for part in parts
                    ])
        for thread_list in threads:
            for thread in thread_list:
                thread.start()
                thread.join()
        for thread_list in threads:
            result_list = []
            user_id = None
            for thread in thread_list:
                user_id = thread.user_id
                result_list.extend(thread.result[1])
            result_list.sort(key=lambda x: x[1], reverse=True)
            result.append([user_id, result_list])
        for pk, vect in result:
            self.model[pk] = vect

    def cos_similarity_vect(self, u, v):
        norm_u = np.sqrt(np.sum(np.power(u, 2)))
        norm_v = np.sqrt(np.sum(np.power(v, 2)))
        dot_uv = np.dot(u, v)
        cos_uv = dot_uv / (norm_u * norm_v) if norm_u * norm_v !=0 else 0
        return cos_uv
    
    def get_recommanded_tracks(self, user_id, similar_users_id):
        track_list = []
        result = []
        user_id_track_list = list(set(self.get_history_user(user_id, self.history_list)))
        for pk in similar_users_id:
            track_list.extend(list(set(self.get_history_user(pk, self.history_list))))
        comon_track = track_list
        track_list = list(set(track_list))
        for pk in track_list:
            if pk not in user_id_track_list:
                s = 0
                for i in comon_track:
                    if i == pk:
                        s += 1
                track = self.get_track(pk, self.track_list)
                result.append([pk, self.temporal_weight(track.created_at), s, s * self.temporal_weight(track.created_at)])
        result.sort(key=lambda x: x[3], reverse=True)
        print(np.array(result))
        result = [i[0] for i in result]
        return result
    
    def get_recommandation(self, user_id, model=None, K=26):
        if not model:
            model = self.model
        similar_users = np.array(model[user_id])[:K, 0]
        print(similar_users)
        recommanded_tracks = self.get_recommanded_tracks(user_id, similar_users)
        return recommanded_tracks
    
    def transpose_data(self):
        result = []
        for track in self.track_list:
            user_track_note_list = []
            for user in self.user_list:
                note_user_track = self.resolve_note(user.pk, track.pk)
                user_track_note_list.append([user.pk, note_user_track])
            result.append([track.pk, user_track_note_list])
        self.transposed_data = result
    
    def get_similar_track(self, track_pk):
        result = []
        for track_id, vect in self.transposed_data:
            track_to_track_similarity_list = []
            if track_id == track_pk:
                for _track_id, _vect in self.transposed_data:
                    track_to_track_similarity = \
                        self.cos_similarity_vect(np.array(vect)[:, 1], np.array(_vect)[:, 1])
                    track_to_track_similarity_list.append([_track_id, track_to_track_similarity])
                track_to_track_similarity_list = \
                    sorted(track_to_track_similarity_list, key=lambda x: x[1], reverse=True)
                result.append([track_id, track_to_track_similarity_list])
                break
        return np.array(result[0][1])[:, 0].tolist()
