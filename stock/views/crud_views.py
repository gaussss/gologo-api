from ast import Delete
import base64
import this
import random
import numpy as np

from django.shortcuts import render
from urllib import request
from django.contrib.auth.models import User
from rest_framework.decorators import action
from django.contrib.auth.hashers import make_password
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from django.core.files.base import ContentFile
from django_filters.rest_framework import DjangoFilterBackend

from core.models import UserDetail
from core.serializers import UserDetailSerializer
from stock.serializers.crud_serializers import (
    AlbumSerializer,
    TrackSerializer,
    HistorySerializer,
    CategorySerializer,
    PlayListSerializer,
    HomeDiscSerializer
)
from stock.models import (
    Album,
    Track,
    History,
    Category,
    PlayList,
    HomeDisc
)
from core.custom_responses import (
    UnAutorizedRequest,
    GoodResponse,
    BadRequest
)
from core.custom_viewset import CRUDViewset
from .recommendation import FiltrageColab
all_track_id = [track.pk for track in Track.objects.all()]


def get_query_request(query_string):
    data = query_string.split('&')
    result = {}
    for item in data:
        key, value = item.split('=')
        result[key] = value
    return result



def index(request):
	return render(request, 'index.html', {})

@api_view(['POST'])
def similar_track(request):
    rcf = FiltrageColab(User, Track, History)
    rcf.load_data()
    rcf.preprocess_data()
    rcf.resolve_data_similarity()
    rcf.transpose_data()
    similar_tracks = rcf.get_similar_track(request.data['track'])[:10]
    similar_tracks = [track for track in similar_tracks if track != request.data['track']]
    data = []
    serialized = []
    for i in similar_tracks:
        try:
            serialized = TrackSerializer(Track.objects.get(pk=i))
            if serialized.is_valid:
                data.append(serialized.data)
        except:
            pass
    return GoodResponse(data)

@api_view(['POST'])
def recommendation(request):
    rcf = FiltrageColab(User, Track, History)
    rcf.load_data()
    rcf.preprocess_data()
    rcf.resolve_data_similarity()
    predicted_track = rcf.get_recommandation(request.data['user']) # request.data['user']
    data = []
    for i in predicted_track:
        track = Track.objects.get(pk=i)
        try:
            data.append(Track.objects.get(pk=i))
        except:
            pass
    serialized = TrackSerializer(data, many=True)
    if serialized.is_valid:
        pass
    return GoodResponse(serialized.data)

class AlbumViewSet(CRUDViewset):
    url_prefix = 'album'
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        # current_user = User.objects.get(pk=request.META["user"])
        # self.queryset = Album.objects.filter(auteur=current_user)
        if request.META['QUERY_STRING'] != '':
            try:
                user = get_query_request(request.META['QUERY_STRING'])['user']
            except Exception as e:
                user = None
                style = get_query_request(request.META['QUERY_STRING'])['style']
            except:
                user = None if user else user
                style = None if style else style
            
            if user != None:
                self.queryset = Album.objects.filter(auteur__pk=int(user))
                return super().list(request, *args, **kwargs)
            elif int(style) == -1:
                return super().list(request, *args, **kwargs)
            self.queryset = Album.objects.filter(tracks__style=style)
        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        AlbumSerializer.Meta.depth = 0

        data = request.data
        format, imgstr = data['image'].split(';base64,') 
        ext = format.split('/')[-1] 
        file = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        request.data['image'] = file
        wrapper = super().create(request, *args, **kwargs)
        AlbumSerializer.Meta.depth = 1
        return wrapper
    
    def update(self, request, pk, *args, **kwargs):
        AlbumSerializer.Meta.depth = 0

        data = request.data
        format, imgstr = data['image'].split(';base64,') 
        ext = format.split('/')[-1] 
        file = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        request.data['image'] = file
        wrapper = super().update(request, pk, *args, **kwargs)
        AlbumSerializer.Meta.depth = 1
        return wrapper


class TrackViewSet(CRUDViewset):
    url_prefix = 'track'
    queryset = Track.objects.all()
    serializer_class = TrackSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=True, methods=['POST'])
    def make_favorite(self, request, pk):
        user = UserDetail.objects.get(user__pk=request.data['user'])
        track = Track.objects.get(pk=pk)
        user.favory.add(track)

        return GoodResponse('great')
    
    @action(detail=True, methods=['GET'])
    def get_favorite(self, request, pk):
        UserDetailSerializer.Meta.depth = 3
        user = UserDetail.objects.get(user__pk=pk)
        serializer = UserDetailSerializer(user)
        if serializer.is_valid:
            pass
        result = GoodResponse(serializer.data['favory'])
        UserDetailSerializer.Meta.depth = 0
        return result
    
    @action(detail=True, methods=['POST'])
    def remove_favorite(self, request, pk):
        user = UserDetail.objects.get(user__pk=request.data['user'])
        track = Track.objects.get(pk=pk)
        user.favory.remove(track)

        return GoodResponse('great')

    def list(self, request, *args, **kwargs):
        if request.META['QUERY_STRING'] != '':
            style = get_query_request(request.META['QUERY_STRING'])['style']
            self.queryset = Track.objects.filter(style=style)
        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        TrackSerializer.Meta.depth = 0

        data = request.data
        formats, imgstr = data['song'].split(';base64,')
        ext = formats.split('/')[-1]
        file = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)

        request.data['song'] = file
        liste = []
        for id in data['style']:
            liste.append(
                Category.objects.get(pk=id)
            )
        request.data['album'] = Album.objects.get(pk=request.data['album'])
        del data['style']
        track = Track.objects.create(**request.data)
        track.style.set(liste)
        track.save()
        wrapper = GoodResponse('created')
        TrackSerializer.Meta.depth = 2
        return wrapper
    
    def update(self, request, pk, *args, **kwargs):
        TrackSerializer.Meta.depth = 0

        data = request.data
        if data['song']:
            formats, imgstr = data['song'].split(';base64,')
            ext = formats.split('/')[-1] 
            file = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
            request.data['song'] = file
        else:
            del data['song']

        wrapper = super().update(request, pk, *args, **kwargs)
        TrackSerializer.Meta.depth = 2
        return wrapper


class DiscoverViewSet(CRUDViewset):
    url_prefix = 'discover'
    queryset = Track.objects.all().order_by('-created_at')[:10]
    serializer_class = TrackSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        if request.META['QUERY_STRING'] != '':
            style = get_query_request(request.META['QUERY_STRING'])['style']
            if int(style) == -1:
                return super().list(request, *args, **kwargs)
            self.queryset = Track.objects.filter(style=style).order_by('-created_at')[:10]
        return super().list(request, *args, **kwargs)


class Top50ViewSet(CRUDViewset):
    url_prefix = 'top50'
    queryset = Track.objects.all().order_by('-see')
    serializer_class = TrackSerializer
    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        serializer = TrackSerializer(self.queryset[:50], many=True)
        if request.META['QUERY_STRING'] != '':
            style = get_query_request(request.META['QUERY_STRING'])['style']
            if int(style) == -1:
                return GoodResponse(serializer.data)
            self.queryset = Track.objects.filter(style=style)
            serializer = TrackSerializer(self.queryset[:50], many=True)
        return GoodResponse(serializer.data)


class Top50AlbumViewSet(CRUDViewset):
    url_prefix = 'top50album'
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    # permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        serializer = AlbumSerializer(self.queryset[:50], many=True)
        if request.META['QUERY_STRING'] != '':
            style = get_query_request(request.META['QUERY_STRING'])['style']
            if int(style) == -1:
                return GoodResponse(serializer.data)
            self.queryset = Album.objects.filter(tracks__style=style)
            serializer = AlbumSerializer(self.queryset[:50], many=True)
        return GoodResponse(serializer.data)


class SearchViewSet(CRUDViewset):
    url_prefix = 'search'
    queryset = Track.objects.all()
    serializer_class = TrackSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        if request.data["props"] == "":
            return GoodResponse("Empty")
        if request.META['QUERY_STRING'] != '':
            style = get_query_request(request.META['QUERY_STRING'])['style']
            if int(style) == -1:
                self.queryset = Track.objects.filter(title__icontains=request.data["props"])
                return super().list(request, *args, **kwargs)
            self.queryset = Track.objects.filter(style=style, title__icontains=request.data["props"])
        return super().list(request, *args, **kwargs)


class HistoryViewSet(CRUDViewset):
    url_prefix = 'history'
    queryset = History.objects.all().order_by('-created_at')
    serializer_class = HistorySerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        HistorySerializer.Meta.depth = 3
        result = super().list(request, *args, **kwargs)
        if request.META['QUERY_STRING'] != '':
            user = get_query_request(request.META['QUERY_STRING'])['user']
            if int(user) == -1:
                return result
            self.queryset = History.objects.filter(user__pk=user).exclude(track__pk=None).order_by('-created_at')
        result = super().list(request, *args, **kwargs)
        HistorySerializer.Meta.depth = 0
        return result

    def create(self, request, *args, **kwargs):
        Track.add_see(request.data["track"])
        return super().create(request, *args, **kwargs)


class CategoryViewSet(CRUDViewset):
    url_prefix = 'category'
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]


class PlayListViewSet(CRUDViewset):
    url_prefix = 'playlist'
    queryset = PlayList.objects.all()
    serializer_class = PlayListSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        result = super().list(request, *args, **kwargs)
        if request.META['QUERY_STRING'] != '':
            user = get_query_request(request.META['QUERY_STRING'])['user']
            if int(user) == -1:
                return result
            self.queryset = PlayList.objects.filter(user__pk=user).exclude(track__pk=None).order_by('-created_at')
        result = super().list(request, *args, **kwargs)
        return result


class HomeDiscViewSet(CRUDViewset):
    url_prefix = 'home-disc'
    queryset = HomeDisc.objects.all()
    serializer_class = HomeDiscSerializer
    permission_classes = [IsAuthenticated]
