from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import crud_views as crv
from .views import bussiness_views as bsv

crud_viewsets = (crv.AlbumViewSet, crv.TrackViewSet,
                crv.HistoryViewSet, crv.DiscoverViewSet,
                crv.SearchViewSet, crv.Top50ViewSet,
                crv.Top50AlbumViewSet, crv.CategoryViewSet,
                crv.PlayListViewSet, crv.HomeDiscViewSet)

router = SimpleRouter()
for viewset in crud_viewsets:
    router.register(r'{}'.format(viewset.url_prefix), viewset, basename=viewset.url_prefix)

urlpatterns = [
    path('similar/', crv.similar_track, name='similar'),
    path('rec/', crv.recommendation, name='rec'),
]
urlpatterns += router.urls
