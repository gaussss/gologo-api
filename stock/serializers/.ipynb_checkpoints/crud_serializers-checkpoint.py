from rest_framework import serializers
from stock.models import (
    Album,
    Track,
    History
)


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = '__all__'
        depth = 2
    
    def create(self, validated_data):
        return Track.objects.create(**validated_data)


class AlbumSerializer(serializers.ModelSerializer):
    tracks = TrackSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        fields = ['name', 'description', 'image', 'auteur',
                'is_active', 'created_at', 'updated_at', 'pk', 'tracks']
        depth = 1
    
    def create(self, validated_data):
        return Album.objects.create(**validated_data)


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = '__all__'
